
CREATE TABLE public.accounts (
	id serial primary key,
	account_number varchar(40) NOT NULL,
	balance numeric(10,2) NOT NULL	
);

CREATE TABLE public.users (
	id serial primary key,
	email_address varchar(128) NOT NULL,
	first_name varchar(64) NOT NULL,
	last_name varchar(64) NOT NULL,
	account_id int null ,
	CONSTRAINT users_email_address_key UNIQUE (email_address),
	foreign key (account_id) references accounts(id)
);


