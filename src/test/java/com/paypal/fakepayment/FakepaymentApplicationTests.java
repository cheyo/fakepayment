package com.paypal.fakepayment;

import com.paypal.fakepayment.service.PaymentSystemImpl;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import static org.junit.Assert.* ;

class FakepaymentApplicationTests {

	@Test
	public void testHappyPath() throws PaymentSystemException {

		PaymentSystem paymentSystem = new PaymentSystemImpl();

		assertNull(paymentSystem.getAccountManager()
				.findAccountsByFirstName(null));
		assertNotNull(paymentSystem.getAccountManager()
				.findAccountsByFirstName(""));
		assertFalse(paymentSystem.getAccountManager()
				.findAccountsByFirstName("").hasNext());
		assertNotNull(paymentSystem.getAccountManager()
				.findAccountsByLastName(""));
		assertNull(paymentSystem.getAccountManager()
				.findAccountsByLastName(null));
		assertFalse(paymentSystem.getAccountManager()
				.findAccountsByLastName("").hasNext());
		assertNotNull(paymentSystem.getAccountManager()
				.findAccountsByFullName("", ""));
		assertNull(paymentSystem.getAccountManager()
				.findAccountsByFullName(null, ""));
		assertNull(paymentSystem.getAccountManager()
				.findAccountsByFullName("", null));
		assertFalse(paymentSystem.getAccountManager()
				.findAccountsByFullName("", "").hasNext());

		PaymentSystemUser user1 = paymentSystem.getUserManager().createUser(
				"John", "Doe", "john.doe@host.net");
		PaymentSystemUser user2 = paymentSystem.getUserManager().createUser(
				"Jane", "Doe", "jane.doe@host.net");
		PaymentSystemUser user3 = paymentSystem.getUserManager().createUser(
				"Gene", "Smith", "gene.smith@host.net");
		PaymentSystemUser user4 = paymentSystem.getUserManager().createUser(
				"John", "Johnson", "john.Johnson@host.net");

		PaymentSystemAccount account1 = paymentSystem.getAccountManager()
				.createAccount(user1, 10);
		PaymentSystemAccount account2 = paymentSystem.getAccountManager()
				.createAccount(user3, 0);
		PaymentSystemAccount account3 = paymentSystem.getAccountManager()
				.createAccount(user4, 0);

		assertNotNull(account1);
		assertNotNull(account2);
		assertNotNull(account3);
		assertEquals((double) 10, java.util.Optional.ofNullable(account1.getAccountBalance()));
		assertEquals((double) 0, java.util.Optional.ofNullable(account2.getAccountBalance()));
		assertEquals((double) 0, java.util.Optional.ofNullable(account3.getAccountBalance()));

		paymentSystem.getAccountManager().addUserToAccount(user2,
				account1.getAccountNumber());

		int numAccounts = 0;
		for (Iterator<PaymentSystemAccount> allAccounts = paymentSystem
				.getAccountManager().getAllAccounts(); allAccounts.hasNext(); allAccounts
					 .next()) {
			numAccounts++;
		}

		assertEquals(3, numAccounts);

		assertEquals(account1, paymentSystem.getAccountManager()
				.getUserAccount(user1));
		assertEquals(account1, paymentSystem.getAccountManager()
				.getUserAccount(user2));
		assertEquals(account2, paymentSystem.getAccountManager()
				.getUserAccount(user3));
		assertEquals(account3, paymentSystem.getAccountManager()
				.getUserAccount(user4));

		paymentSystem.sendMoney(user1, user3, 5);

		assertEquals((double) 5, java.util.Optional.ofNullable(account1.getAccountBalance()));
		assertEquals((double) 5, java.util.Optional.ofNullable(account2.getAccountBalance()));
		assertEquals((double) 0, java.util.Optional.ofNullable(account3.getAccountBalance()));

		paymentSystem.sendMoney(user2, user3, 5);

		assertEquals((double) 0, java.util.Optional.ofNullable(account1.getAccountBalance()));
		assertEquals((double) 10, java.util.Optional.ofNullable(account2.getAccountBalance()));
		assertEquals((double) 0, java.util.Optional.ofNullable(account3.getAccountBalance()));

		Set<PaymentSystemUser> to = new HashSet<>();
		to.add(user1);
		to.add(user2);

		paymentSystem.sendMoney(user3, to, 5);

		assertEquals((double) 10, java.util.Optional.ofNullable(account1.getAccountBalance()));
		assertEquals((double) 0, java.util.Optional.ofNullable(account2.getAccountBalance()));
		assertEquals((double) 0, java.util.Optional.ofNullable(account3.getAccountBalance()));

		to.clear();
		to.add(user3);
		to.add(user4);

		paymentSystem.distributeMoney(user2, to, 10);

		assertEquals((double) 0, java.util.Optional.ofNullable(account1.getAccountBalance()));
		assertEquals((double) 5, java.util.Optional.ofNullable(account2.getAccountBalance()));
		assertEquals((double) 5, java.util.Optional.ofNullable(account3.getAccountBalance()));
	}
}
