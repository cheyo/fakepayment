package com.paypal.fakepayment.rest;

import com.paypal.fakepayment.PaymentSystemCustomer;
import com.paypal.fakepayment.PaymentSystemCustomerManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/fakepay/")
public class CustomerController {

    @Autowired
    private PaymentSystemCustomerManager service ;

    @GetMapping("/customerlist")
    public ResponseEntity<List<PaymentSystemCustomer>> getCustomerList() {
        return new ResponseEntity<>(service.getCustomerList(), HttpStatus.OK) ;
    }

}
