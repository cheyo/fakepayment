package com.paypal.fakepayment.rest.request;

import java.util.List;
import java.util.Set;

public class SendMoneyRequest {

    private String from ;
    private List<String> to ;
    private double amount ;

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public List<String> getTo() {
        return to;
    }

    public void setTo(List<String> to) {
        this.to = to;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
