package com.paypal.fakepayment.rest;

import com.paypal.fakepayment.PaymentSystemException;
import com.paypal.fakepayment.service.impl.UsersPaymentSystemImpl;
import com.paypal.fakepayment.rest.request.UsersPaymentSystemRequest;
import com.paypal.fakepayment.service.PaymentSystemUserManagerImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;


@RestController
@RequestMapping("/api/fakepay/users")
public class UsersPaymentController {

    @Autowired
    private PaymentSystemUserManagerImpl service ;

    @GetMapping
    public ResponseEntity<List<UsersPaymentSystemImpl> > getAll(){
        return new ResponseEntity(service.getAll(), HttpStatus.OK) ;
    }

    @PostMapping
    public ResponseEntity<UsersPaymentSystemImpl>addUser(@RequestBody UsersPaymentSystemRequest request){
        try {
            return new ResponseEntity(
                    service.createUser(request.getFirstName(),
                            request.getLastName(),
                            request.getEmailAddress()), HttpStatus.OK);
                     }
        catch (PaymentSystemException e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e) ;
        }
    }
}
