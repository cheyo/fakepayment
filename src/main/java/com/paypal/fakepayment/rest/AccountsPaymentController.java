package com.paypal.fakepayment.rest;

import com.paypal.fakepayment.PaymentSystemAccount;
import com.paypal.fakepayment.PaymentSystemAccountManager;
import com.paypal.fakepayment.PaymentSystemException;
import com.paypal.fakepayment.service.impl.UsersPaymentSystemImpl;
import com.paypal.fakepayment.rest.request.AccountPaymentSystemRequest;
import com.paypal.fakepayment.rest.request.FindAccountRequest;
import com.paypal.fakepayment.util.ResponseMessages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Iterator;

@RestController
@RequestMapping("/api/fakepay/accounts")
public class AccountsPaymentController {
    @Autowired
    private PaymentSystemAccountManager service ;

    @PostMapping
    public ResponseEntity<PaymentSystemAccount> createAccount(@RequestBody AccountPaymentSystemRequest request){

        UsersPaymentSystemImpl user = new UsersPaymentSystemImpl(request.getEmailAddress());

        try {
            return new ResponseEntity(
                    service.createAccount(user, request.getBalance()), HttpStatus.OK);
        }catch (PaymentSystemException e) {
                e.printStackTrace();
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e) ;
        }
    }

    @PostMapping("/users")
    public ResponseEntity<PaymentSystemAccount> addUserToAccount(@RequestBody AccountPaymentSystemRequest request){
        UsersPaymentSystemImpl user = new UsersPaymentSystemImpl(request.getEmailAddress());
        try {
            return new ResponseEntity(
                    service.addUserToAccount(user, request.getAccountNumber()), HttpStatus.OK);
        }catch (PaymentSystemException e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e) ;
        }
    }

    @GetMapping
    public ResponseEntity<Iterator<PaymentSystemAccount>> getAllAccounts() {
            return new ResponseEntity(
                    service.getAllAccounts(), HttpStatus.OK);
    }

    @PostMapping("/find")
    public ResponseEntity<Iterator<PaymentSystemAccount>> find(@RequestBody FindAccountRequest request){

            if (request.getFirstName()!= null && request.getLastName() == null ){
                return new ResponseEntity(
                        service.findAccountsByFirstName(request.getFirstName()), HttpStatus.OK);
            }

            if (request.getFirstName()!= null && request.getLastName() != null ){
                return new ResponseEntity(
                        service.findAccountsByFullName(request.getFirstName(), request.getLastName()), HttpStatus.OK);
            }

            if (request.getFirstName()== null && request.getLastName() != null ){
                return new ResponseEntity(
                        service.findAccountsByLastName( request.getLastName()), HttpStatus.OK);
            }

            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ResponseMessages.SHOULD_SPECIFY_FIND_KEY) ;

    }

}
