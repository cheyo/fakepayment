package com.paypal.fakepayment.rest;

import com.paypal.fakepayment.PaymentSystem;
import com.paypal.fakepayment.PaymentSystemException;
import com.paypal.fakepayment.PaymentSystemUser;
import com.paypal.fakepayment.rest.request.SendMoneyRequest;
import com.paypal.fakepayment.service.impl.UsersPaymentSystemImpl;
import com.paypal.fakepayment.util.ResponseMessages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashSet;
import java.util.Set;

@RestController
@RequestMapping("/api/fakepay/")
public class PaymentSystemController {

    @Autowired
    private PaymentSystem service;

    @PostMapping("/sendmoney")
    public ResponseEntity<Object> sendMoney(@RequestBody SendMoneyRequest request) {

        try {
            PaymentSystemUser from = new UsersPaymentSystemImpl(request.getFrom());
            if (request.getTo().size() == 1) {
                PaymentSystemUser to = new UsersPaymentSystemImpl(request.getTo().iterator().next());

                service.sendMoney(from, to, request.getAmount());

                return new ResponseEntity<>(ResponseMessages.SEND_MONEY_PROCESSED , HttpStatus.OK) ;
            } else {

                Set<PaymentSystemUser> toSet = new HashSet<>();
                for (String item : request.getTo()) {
                    toSet.add(new UsersPaymentSystemImpl(item));
                }
                service.sendMoney(from, toSet, request.getAmount());

                return new ResponseEntity<>(ResponseMessages.SEND_MONEY_PROCESSED , HttpStatus.OK) ;
            }

        } catch (PaymentSystemException e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage()) ;
        }
    }

}
