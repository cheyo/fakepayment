package com.paypal.fakepayment.util;

public class ResponseMessages {

    public static final String RECORD_NOT_NULL = "%s must not be null." ;
    public static final String EMAIL_NOT_VALID = "%s not a valid email" ;
    public static final String EMAIL_ALREADY_EXISTS = "Email %s already exists." ;
    public static final String CANNOT_CREATE_RECORD = "Cannot create %s. Contact Support" ;
    public static final String EMAIL_ADDRESS_NOT_EXISTS = "Email %s does not exists as user in system. You should register first." ;
    public static final String ACCOUNT_NOT_EXISTS = "Account %s does not exists as user in system. You should register first." ;
    public static final String USER_ALREADY_ACCOUNT = "User already link to an account. Please try another email.";
    public static final String VALUE_LESS_ZERO = "Amount is less than 0";
    public static final String SHOULD_SPECIFY_FIND_KEY = "You should specify a find key" ;
    public static final String USER_HAS_NO_BALANCE = "User has no balance.";
    public static final String BALANCE_NOT_ENOUGH = "Balance not enough";
    public static final String SENDMONEY_CANNOT_BE_PROCESSED = "There was an error sending money thru accounts.";
    public static final String CANT_TRANSFER_SAME_ACCOUNTS = "Cannot send money from same accounts";
    public static final Object SEND_MONEY_PROCESSED = "Money Send Correctly!";
}
