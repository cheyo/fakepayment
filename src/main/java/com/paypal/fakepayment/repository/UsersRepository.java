package com.paypal.fakepayment.repository;

import com.paypal.fakepayment.domain.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UsersRepository extends JpaRepository<Users, Long> {

    List<Users> findAll() ;

    Optional<Users> findByEmailAddress(String emailAddress) ;

    List<Users> findByFirstNameAndLastnameAndAccountIdIsNotNull(String firstName, String lastName);

    List<Users> findByFirstNameAndAccountIdIsNotNull(String firstName) ;

    List<Users> findByLastnameAndAccountIdIsNotNull(String lastName) ;

}
