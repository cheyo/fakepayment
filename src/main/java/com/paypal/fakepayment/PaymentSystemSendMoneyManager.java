package com.paypal.fakepayment;

import com.paypal.fakepayment.service.PaymentSystemAccountManagerImpl;

import java.util.List;

public interface PaymentSystemSendMoneyManager {

    public PaymentSystemAccountManagerImpl sendMoney(PaymentSystemAccount fromUser, List<PaymentSystemAccount> toUsers, double amount)
            throws PaymentSystemException;

}
