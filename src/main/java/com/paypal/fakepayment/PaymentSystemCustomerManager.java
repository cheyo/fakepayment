package com.paypal.fakepayment;

import java.util.List;

public interface PaymentSystemCustomerManager {

    public List<PaymentSystemCustomer> getCustomerList();

}
