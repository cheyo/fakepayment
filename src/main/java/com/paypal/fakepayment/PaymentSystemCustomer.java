package com.paypal.fakepayment;

public interface PaymentSystemCustomer {
    public String getAccountNumber() ;
    public double getBalance();
    public String getFistName() ;
    public String getLastName() ;
    public String getEmailAddress() ;
}
