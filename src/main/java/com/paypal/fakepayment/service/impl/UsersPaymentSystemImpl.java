package com.paypal.fakepayment.service.impl;

import com.paypal.fakepayment.PaymentSystemUser;
import com.sun.istack.NotNull;

public class UsersPaymentSystemImpl implements PaymentSystemUser {

    private Long id ;
    @NotNull
    private String firstName ;
    @NotNull
    private String lastName ;
    @NotNull
    private String emailAddress ;

    public UsersPaymentSystemImpl(Long id, String firstName, String lastName, String emailAddres) {
        this.emailAddress = emailAddres ;
        this.firstName = firstName ;
        this.lastName = lastName ;
        this.id = id ;
    }

    public UsersPaymentSystemImpl(String firstName, String lastName, String emailAddres) {
        this.emailAddress = emailAddres ;
        this.firstName = firstName ;
        this.lastName = lastName ;
    }

    public UsersPaymentSystemImpl(String emailAddress){
        this.emailAddress = emailAddress ;
    }

    @Override
    public String getFirstName() {
        return this.firstName;
    }

    @Override
    public String getLastName() {
        return this.lastName;
    }

    @Override
    public String getEmailAddress() {
        return this.emailAddress;
    }

    public Long getId() {
        return id;
    }
}
