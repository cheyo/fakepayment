package com.paypal.fakepayment.service.impl;

import com.paypal.fakepayment.PaymentSystemCustomer;

public class CustomerPaymentSystemImpl implements PaymentSystemCustomer {
    private String accountNumber;
    private String firstName;
    private String lastName ;
    private String emailAddres ;
    private double balance ;

    public CustomerPaymentSystemImpl(String accountNumber, String firstName, String lastName, String emailAddres, double balance) {
        this.accountNumber = accountNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.emailAddres = emailAddres;
        this.balance = balance;
    }

    @Override
    public String getAccountNumber() {
        return accountNumber;
    }

    @Override
    public double getBalance() {
        return balance;
    }

    @Override
    public String getFistName() {
        return firstName;
    }

    @Override
    public String getLastName() {
        return lastName;
    }

    @Override
    public String getEmailAddress() {
        return emailAddres;
    }
}
