package com.paypal.fakepayment.service;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.paypal.fakepayment.PaymentSystemAccount;
import com.paypal.fakepayment.PaymentSystemException;
import com.paypal.fakepayment.PaymentSystemUser;
import com.paypal.fakepayment.domain.Accounts;
import com.paypal.fakepayment.repository.AccountsRepository;
import com.paypal.fakepayment.service.impl.UsersPaymentSystemImpl;
import com.paypal.fakepayment.util.ResponseMessages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Service
public class AccountsPaymentSystemImpl implements PaymentSystemAccount {

    private  Accounts account ;

    @Autowired
    private AccountsRepository repository ;

    public AccountsPaymentSystemImpl(){}

    public void setAccount(Accounts account) {
        this.account = account;
    }

    public AccountsPaymentSystemImpl(Accounts accounts) {
        this.account = accounts ;
    }

    @Override
    public String getAccountNumber() {
        return account.getAccountNumber();
    }

    @Override
    public Double getAccountBalance() {
        return account.getBalance();
    }

    @Override
    public void incrementAccountBalance(Double amount) throws PaymentSystemException {
        if (amount < 0){
            throw new PaymentSystemException(ResponseMessages.VALUE_LESS_ZERO);
        }
        this.account.setBalance(this.getAccountBalance() + amount);

        repository.save(this.account) ;
    }

    @Override
    public void decrementAccountBalance(Double amount) throws PaymentSystemException {

        if (account.getBalance() == 0){
            throw new PaymentSystemException("Your balance is 0.00");
        }

        if ( (account.getBalance() - amount) < 0 ) {
            throw new PaymentSystemException("You don't have enough balance");
        }

        this.account.setBalance( this.account.getBalance() - amount );

        repository.save(this.account) ;

    }

    @Override
    public Iterator<PaymentSystemUser> getAccountUsers() {

        List<PaymentSystemUser> list = new ArrayList<>();
                account.getUsers().stream()
                .forEach(u -> {
                    UsersPaymentSystemImpl usr = new UsersPaymentSystemImpl(u.getFirstName(), u.getLastname(), u.getEmailAddress());
                    list.add(usr) ;
                });

        return list.iterator();
    }

    public Accounts getAccount() {
        return account;
    }

}
