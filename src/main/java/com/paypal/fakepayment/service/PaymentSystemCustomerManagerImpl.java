package com.paypal.fakepayment.service;

import com.paypal.fakepayment.PaymentSystemCustomer;
import com.paypal.fakepayment.PaymentSystemCustomerManager;
import com.paypal.fakepayment.domain.Users;
import com.paypal.fakepayment.repository.UsersRepository;
import com.paypal.fakepayment.service.impl.CustomerPaymentSystemImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PaymentSystemCustomerManagerImpl implements PaymentSystemCustomerManager {

    @Autowired
    private UsersRepository usersRepository ;

    @Override
    public List<PaymentSystemCustomer> getCustomerList() {

        List<Users> userFromDb = usersRepository.findAll() ;
        List<PaymentSystemCustomer> response = new ArrayList<>();

        userFromDb.forEach(u -> {
            PaymentSystemCustomer customer = new CustomerPaymentSystemImpl(
                    u.getAccountId().getAccountNumber(),
                    u.getFirstName(),
                    u.getLastname(),
                    u.getEmailAddress(),
                    u.getAccountId().getBalance()
            );
            response.add(customer) ;
        });


        return response;
    }
}
