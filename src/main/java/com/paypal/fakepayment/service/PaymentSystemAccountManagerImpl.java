package com.paypal.fakepayment.service;

import com.paypal.fakepayment.PaymentSystemAccount;
import com.paypal.fakepayment.PaymentSystemAccountManager;
import com.paypal.fakepayment.PaymentSystemException;
import com.paypal.fakepayment.PaymentSystemUser;
import com.paypal.fakepayment.domain.Accounts;
import com.paypal.fakepayment.domain.Users;
import com.paypal.fakepayment.repository.AccountsRepository;
import com.paypal.fakepayment.repository.UsersRepository;
import com.paypal.fakepayment.service.impl.UsersPaymentSystemImpl;
import com.paypal.fakepayment.util.ResponseMessages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class PaymentSystemAccountManagerImpl implements PaymentSystemAccountManager {
    @Autowired
    private UsersRepository usersRepository ;
    @Autowired
    private AccountsRepository accountsRepository ;

    @Override
    public PaymentSystemAccount createAccount(PaymentSystemUser user, double initialBalance)
            throws PaymentSystemException {

        if (validate(user.getEmailAddress())){
            Optional<Users> userFromDb = usersRepository.findByEmailAddress(user.getEmailAddress()) ;

            if (userFromDb.isPresent()){
                Users usrFromDb = userFromDb.get();
                if (usrFromDb.getAccountId()!= null){
                    throw new PaymentSystemException(ResponseMessages.USER_ALREADY_ACCOUNT);
                }

                Accounts account = new Accounts();
                account.setAccountNumber(UUID.randomUUID().toString());
                account.setBalance(initialBalance);

                account = accountsRepository.save(account) ;

                usrFromDb.setAccountId(account);
                usersRepository.save(usrFromDb) ;

                AccountsPaymentSystemImpl response = new AccountsPaymentSystemImpl(account);

                return response ;
            }else {
                throw new PaymentSystemException(String.format(ResponseMessages.EMAIL_ADDRESS_NOT_EXISTS, user.getEmailAddress()));
            }

        }

        throw new PaymentSystemException(String.format(ResponseMessages.CANNOT_CREATE_RECORD, "Account"));
    }


    @Override
    public PaymentSystemAccount addUserToAccount(PaymentSystemUser user, String accountNumber)
            throws PaymentSystemException {

        if (validate(user.getEmailAddress(), accountNumber)){
            Optional<Users> userFromDb = usersRepository.findByEmailAddress(user.getEmailAddress()) ;
            if (userFromDb.isPresent()){
                Optional<Accounts> accountFromDb = accountsRepository.findByAccountNumber(accountNumber);
                if (accountFromDb.isPresent()){
                    Users usrToDb = userFromDb.get() ;
                    usrToDb.setAccountId(accountFromDb.get());

                    usersRepository.save(usrToDb) ;
                    accountsRepository.flush();

                    AccountsPaymentSystemImpl response = new AccountsPaymentSystemImpl(accountFromDb.get()) ;

                    return response ;
                }else {
                    throw new PaymentSystemException(String.format(ResponseMessages.ACCOUNT_NOT_EXISTS, accountNumber));
                }

            }else {
                throw new PaymentSystemException(String.format(ResponseMessages.EMAIL_ADDRESS_NOT_EXISTS, user.getEmailAddress()));
            }

        }

        return null;
    }

    @Override
    public Iterator<PaymentSystemAccount> getAllAccounts() {

        List<PaymentSystemAccount> response = accountsRepository.findAll().stream().map(a -> {
            AccountsPaymentSystemImpl v = new AccountsPaymentSystemImpl(a) ;
            return  v ;
        }).collect(Collectors.toList());

        Iterator<PaymentSystemAccount> iterator = response.iterator();

        return iterator ;
    }

    @Override
    public PaymentSystemAccount getUserAccount(PaymentSystemUser user) {
        Optional<Users> existUser=  usersRepository.findByEmailAddress(user.getEmailAddress());
        if (existUser.isPresent()) {
            Users userFromDb = existUser.get();
            AccountsPaymentSystemImpl response = new AccountsPaymentSystemImpl(userFromDb.getAccountId());
            return response ;
        }
        return null;
    }

    @Override
    public Iterator<PaymentSystemAccount> findAccountsByFullName(String firstName, String lastName) {

        List<Users> usersFromDb = usersRepository.findByFirstNameAndLastnameAndAccountIdIsNotNull(firstName,lastName);

        List<PaymentSystemAccount> response = new ArrayList<>();

        usersFromDb.stream().forEach(u -> {
            AccountsPaymentSystemImpl accountDto = new AccountsPaymentSystemImpl(u.getAccountId());
            List<PaymentSystemUser> usrAccList = new ArrayList<>() ;
            UsersPaymentSystemImpl usrDto = new UsersPaymentSystemImpl(u.getFirstName(), u.getLastname(), u.getEmailAddress()) ;
            usrAccList.add(usrDto) ;

            //accountDto.setAccountUsers(usrAccList.iterator());
            response.add(accountDto);
        });

        return response.iterator();
    }

    @Override
    public Iterator<PaymentSystemAccount> findAccountsByFirstName(String firstName) {

        List<Users> usersFromDb = usersRepository.findByFirstNameAndAccountIdIsNotNull(firstName);

        List<PaymentSystemAccount> response = new ArrayList<>();

        usersFromDb.stream().forEach(u -> {
            AccountsPaymentSystemImpl accountDto = new AccountsPaymentSystemImpl(u.getAccountId());
            List<PaymentSystemUser> usrAccList = new ArrayList<>() ;
            UsersPaymentSystemImpl usrDto = new UsersPaymentSystemImpl(u.getFirstName(), u.getLastname(), u.getEmailAddress()) ;
            usrAccList.add(usrDto) ;

            //accountDto.setAccountUsers(usrAccList.iterator());
            response.add(accountDto);
        });

        return response.iterator();
    }

    @Override
    public Iterator<PaymentSystemAccount> findAccountsByLastName(String lastName) {
        List<Users> usersFromDb = usersRepository.findByLastnameAndAccountIdIsNotNull(lastName);

        List<PaymentSystemAccount> response = new ArrayList<>();

        usersFromDb.stream().forEach(u -> {
            AccountsPaymentSystemImpl accountDto = new AccountsPaymentSystemImpl(u.getAccountId());
            List<PaymentSystemUser> usrAccList = new ArrayList<>() ;
            UsersPaymentSystemImpl usrDto = new UsersPaymentSystemImpl(u.getFirstName(), u.getLastname(), u.getEmailAddress()) ;
            usrAccList.add(usrDto) ;

            //accountDto.setAccountUsers(usrAccList.iterator());
            response.add(accountDto);
        });

        return response.iterator();
    }

    /**
     * Validates Email ADdress
     * @param emailAddress
     * @return
     * @throws PaymentSystemException
     */
    private boolean validate(String emailAddress) throws PaymentSystemException {

        if (emailAddress == null) {
            throw new PaymentSystemException(String.format(ResponseMessages.RECORD_NOT_NULL, "Email"));
        }

        return true ;
    }

    /**
     * Validates emailAddres and account number
     * @param emailAddress
     * @param accountNumber
     * @return
     * @throws PaymentSystemException
     */
    private boolean validate(String emailAddress, String accountNumber) throws PaymentSystemException {

        validate(emailAddress) ;

        if (accountNumber == null) {
            throw new PaymentSystemException(String.format(ResponseMessages.RECORD_NOT_NULL, "Account Number"));
        }
        return true ;
    }
}
