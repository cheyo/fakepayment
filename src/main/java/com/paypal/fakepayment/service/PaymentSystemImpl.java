package com.paypal.fakepayment.service;

import com.paypal.fakepayment.*;
import com.paypal.fakepayment.domain.Accounts;
import com.paypal.fakepayment.domain.Users;
import com.paypal.fakepayment.util.ResponseMessages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class PaymentSystemImpl implements PaymentSystem {
    @Autowired
    private PaymentSystemUserManagerImpl userManager;
    @Autowired
    private AccountsPaymentSystemImpl accountManager;

    @Override
    public PaymentSystemAccountManager getAccountManager() {
        return null;
    }

    @Override
    public PaymentSystemUserManager getUserManager() {
        return null;
    }

    @Override
    public synchronized void sendMoney(PaymentSystemUser from, PaymentSystemUser to, double amount) throws PaymentSystemException {

        Optional<Users> fromUser = userManager.existsEmailAddress(from.getEmailAddress());
        Optional<Users> toUser = userManager.existsEmailAddress(to.getEmailAddress());

        if (!fromUser.isPresent()) {
            throw new PaymentSystemException(String.format(ResponseMessages.EMAIL_ADDRESS_NOT_EXISTS, from.getEmailAddress()));
        }

        if (!toUser.isPresent()) {
            throw new PaymentSystemException(String.format(ResponseMessages.EMAIL_ADDRESS_NOT_EXISTS, to.getEmailAddress()));
        }

        Accounts userAccountFrom = fromUser.get().getAccountId();
        Accounts userAccountTo = toUser.get().getAccountId();

        if (userAccountFrom.getAccountNumber().equals(userAccountTo.getAccountNumber())) {
            throw new PaymentSystemException(ResponseMessages.CANT_TRANSFER_SAME_ACCOUNTS);
        }


        if (!hasBalance(userAccountFrom)) {
            throw new PaymentSystemException(ResponseMessages.USER_HAS_NO_BALANCE);
        }

        if (balanceIsEnoughForAllDestinations(userAccountFrom, amount)) {

            accountManager.setAccount(userAccountFrom);
            accountManager.decrementAccountBalance(amount);

            accountManager.setAccount(userAccountTo);
            accountManager.incrementAccountBalance(amount);


        } else {
            throw new PaymentSystemException(ResponseMessages.BALANCE_NOT_ENOUGH);
        }


    }

    @Override
    public synchronized void sendMoney(PaymentSystemUser from, Set<PaymentSystemUser> to, double amount) throws PaymentSystemException {
        Optional<Users> fromUser = userManager.existsEmailAddress(from.getEmailAddress());

        if (!fromUser.isPresent()) {
            throw new PaymentSystemException(String.format(ResponseMessages.EMAIL_ADDRESS_NOT_EXISTS, from.getEmailAddress()));
        }

        Accounts userFromAccount = fromUser.get().getAccountId();

        if (balanceIsEnoughForAllDestinations(userFromAccount, to.size(), amount)) {
            List<Accounts> listOfDest = existsAllDestinations(to);

            accountManager.setAccount(userFromAccount);
            accountManager.decrementAccountBalance(amount * to.size());

            listOfDest.forEach(
                    a -> {
                        accountManager.setAccount(a);
                        try {
                            accountManager.incrementAccountBalance(amount);
                        } catch (PaymentSystemException e) {
                            e.printStackTrace();
                        }
                    }
            );

        }else {
            throw new PaymentSystemException(ResponseMessages.BALANCE_NOT_ENOUGH);
        }


    }

    @Override
    public void distributeMoney(PaymentSystemUser from, Set<PaymentSystemUser> to, double amount) throws PaymentSystemException {

    }

    /**
     * Verifies if all destinations exists
     *
     * @param toAccounts
     * @return
     */
    private List<Accounts> existsAllDestinations(Set<PaymentSystemUser> toAccounts) throws PaymentSystemException {
        List<Accounts> listOfDest = new ArrayList<>();

        Iterator<PaymentSystemUser> iterator = toAccounts.iterator();
        while (iterator.hasNext()) {
            PaymentSystemUser u = iterator.next();

            Optional<Users> existInDb = userManager.existsEmailAddress(u.getEmailAddress());
            if (!existInDb.isPresent()) {
                throw new PaymentSystemException(String.format(ResponseMessages.EMAIL_ADDRESS_NOT_EXISTS, u.getEmailAddress()));
            }
            listOfDest.add(existInDb.get().getAccountId());
        }
        return listOfDest;
    }

    /**
     * Verifys if balance is enough for all accounts
     *
     * @param account
     * @param size
     * @param amount
     * @return
     */
    private boolean balanceIsEnoughForAllDestinations(Accounts account, int size, double amount) {
        double totalAmount = amount * size;

        return (totalAmount <= account.getBalance());
    }

    private boolean balanceIsEnoughForAllDestinations(Accounts account, double amount) {

        return (amount <= account.getBalance());
    }

    /**
     * check if balance greater than 0
     *
     * @param account
     * @return
     */
    private boolean hasBalance(Accounts account) {

        return (account.getBalance() > 0);

    }

}
