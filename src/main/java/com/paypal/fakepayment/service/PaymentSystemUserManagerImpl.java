package com.paypal.fakepayment.service;

import com.paypal.fakepayment.domain.Users;
import com.paypal.fakepayment.PaymentSystemException;
import com.paypal.fakepayment.PaymentSystemUser;
import com.paypal.fakepayment.PaymentSystemUserManager;
import com.paypal.fakepayment.repository.UsersRepository;
import com.paypal.fakepayment.rest.request.UsersPaymentSystemRequest;
import com.paypal.fakepayment.service.impl.UsersPaymentSystemImpl;
import com.paypal.fakepayment.util.ResponseMessages;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PaymentSystemUserManagerImpl implements PaymentSystemUserManager {

    @Autowired
    private UsersRepository repository ;

    @Override
    public PaymentSystemUser createUser(String firstName, String lastName, String emailAddress)
            throws PaymentSystemException {

        if (validate(firstName, lastName, emailAddress)) {

            Users user = new Users();
            user.setEmailAddress(emailAddress);
            user.setFirstName(firstName);
            user.setLastname(lastName);

            user = repository.save(user) ;

            UsersPaymentSystemImpl response = new UsersPaymentSystemImpl(user.getId(), user.getFirstName(), user.getLastname(), user.getEmailAddress());

            return response ;
        }

        throw new PaymentSystemException(String.format(ResponseMessages.CANNOT_CREATE_RECORD, "User"));
    }

    /**
     * Validate User's fields
     * @param firstName
     * @param lastName
     * @param emailAddress
     * @return
     * @throws PaymentSystemException
     */
    private boolean validate(String firstName, String lastName, String emailAddress) throws PaymentSystemException {
        if (existsEmailAddress(emailAddress).isPresent())
            throw new PaymentSystemException(String.format(ResponseMessages.EMAIL_ALREADY_EXISTS, emailAddress)) ;

        if (firstName == null){
            throw new PaymentSystemException(String.format(String.format(ResponseMessages.RECORD_NOT_NULL, "First Name"))) ;
        }

        if (lastName == null) {
            throw new PaymentSystemException(String.format(String.format(ResponseMessages.RECORD_NOT_NULL, "Last Name"))) ;
        }

        if (!isValidEmailAddress(emailAddress)){
            throw new PaymentSystemException(String.format(String.format(ResponseMessages.EMAIL_NOT_VALID, emailAddress))) ;
        }

        return true ;
    }

    public static boolean isValidEmailAddress(String email) {
        return  EmailValidator.getInstance().isValid(email);
    }
    /**
     * Create Users
     * @param request
     * @return
     * @throws PaymentSystemException
     */
    public PaymentSystemUser createUser(UsersPaymentSystemRequest request)
        throws PaymentSystemException{

       return createUser(request.getFirstName(), request.getLastName(), request.getEmailAddress());
    }

    /**
     * Check if an email exists in users table
     * @param emailAddress
     * @return
     * @throws PaymentSystemException
     */
    public Optional<Users> existsEmailAddress(String emailAddress) throws PaymentSystemException {
        if (emailAddress == null) {
            throw new PaymentSystemException(String.format(ResponseMessages.RECORD_NOT_NULL, "Email")) ;
        }
        Optional<Users> existsFromDb = repository.findByEmailAddress(emailAddress);
        return existsFromDb ;
    }

    /**
     * Return list of users
     * @return
     */
    public List<PaymentSystemUser> getAll(){
        return repository.findAll().stream().map(e -> {
            UsersPaymentSystemImpl dto = new UsersPaymentSystemImpl(
                    e.getId(),
                    e.getFirstName(),
                    e.getLastname(),
                    e.getEmailAddress());
            return dto ;
        }).collect(Collectors.toList());
    }
}
